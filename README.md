#spring4mvc
springmvc4零配置的基础学习。
基于rest风格去新增更新查找实体对象。
个人的感觉还是挺不错的。
项目代码演示http://tryatest.oschina.mopaasapp.com/user/1
默认user 有4个。可以转换参数去访问。
例如： 
获取所有的用户：
http://tryatest.oschina.mopaasapp.com/user/
获取id为2的用户：
http://tryatest.oschina.mopaasapp.com/user/1

接口提供创建user 删除user  更新user等。
自行尝试。